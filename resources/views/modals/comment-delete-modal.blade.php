<div class="modal fade" id="comment_delete_modal" tabindex="-1" role="dialog" aria-labelledby="basic_modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="sm-message-text text-center">
                <p class="large-text modal-text">Are you sure you want to delete this comment?</p>
            </div>
            <div class="modal-footer">
                <div class="row p-l-15 p-r-15">
                    <a href="{{ route('comments.destroy', encodeId($comment->id)) }}" class="btn btn-danger pull-right m-0 delete_button">Yes</a>
                    <button type="button" class="btn btn-default btn-flat pull-right m-0 p-r-0 p-l-0 m-r-30" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>
</div>
