<!-- Page Header -->
@if(isset($post) && (url()->current() == route('posts.show', $post->id)))
<!-- Page Header -->
<header class="masthead" style="background-image: url('/img/home-bg.jpg')">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
                <div class="site-heading">
                    <h1>SovTech Blog</h1>
                    <span class="subheading">{!! str_limit($post->content, $limit = 150, $end = '...') !!}</span>
                    <p class="post-meta">Posted by
                        <a href="#">{{ $post->owner->name }}</a>
                        on {{ \Carbon\Carbon::parse($post->created_at)->format('Y-m-d') }}
                    </p>
                </div>
            </div>
        </div>
    </div>
</header>
@else
    <header class="masthead" style="background-image: url('/img/home-bg.jpg')">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-10 mx-auto">
                    <div class="site-heading">
                        <h1>SovTech Blog</h1>
                        <span class="subheading">A great way of creating insight and hope</span>
                    </div>
                </div>
            </div>
        </div>
    </header>
@endif