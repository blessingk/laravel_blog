@extends('layouts.app')
@section('content')
    <!-- Main Content -->
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
                <div class="post-preview">
                    <h2 class="post-title">
                        Update Profile
                    </h2>
                    <form action="{{ route('users.update') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" name="name" value="{{ $user->name }}" class="form-control" id="name">
                            @if ($errors->has('name'))
                                <span class="has-error text-danger small">{{ $errors->first('name') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="text" name="email" value="{{ $user->email }}" class="form-control" id="email">
                            @if ($errors->has('email'))
                                <span class="has-error text-danger small">{{ $errors->first('email') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" name="password" class="form-control" id="password">
                            @if ($errors->has('password'))
                                <span class="has-error text-danger small">{{ $errors->first('password') }}</span>
                            @endif
                        </div>

                        <button type="submit" class="btn btn-primary">Update</button>
                    </form>
                </div>
                <hr>
            </div>
        </div>
    </div>

    <hr>
@endsection