@extends('layouts.app')
@section('content')
    <!-- Main Content -->
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
                @foreach($posts as $post)
                    <div class="post-preview">
                        <a href="{{ route('posts.show', encodeId($post->id)) }}">
                            <h3 class="post-title">
                                {{ $post->title }}
                            </h3>
                            <h4 class="post-subtitle">
                                {!! str_limit($post->content, $limit = 150, $end = '...') !!}
                            </h4>
                        </a>
                        <p class="post-meta">Posted by
                            <a href="#">{{ $post->owner->name }}</a>
                            on {{ \Carbon\Carbon::parse($post->created_at)->format('Y-m-d') }}

                            @auth
                                @if(Auth::user()->id == $post->user_id)
                                    <a class="offset-4" href="{{ route('posts.edit', encodeId($post->id)) }}"><i class="fa fa-edit"></i></a>
                                    <a class="m-2 text-danger" href="#" data-toggle="modal" data-target="#post_delete_modal"><i class="fa fa-trash"></i></a>
                                @endif
                            @endauth
                        </p>
                    </div>
                    <hr>
                    @include('modals.post-delete-modal')
                @endforeach
                @if(!$posts->count())
                    <div class="post-preview">
                        <a>
                            <h4 class="post-subtitle">
                                No posts available
                            </h4>
                        </a>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <hr>
@endsection