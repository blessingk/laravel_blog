@extends('layouts.app')
@section('title', 'Post')
@section('content')

    <!-- Post Content -->
    <article>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-10 mx-auto">
                    {!! $post->content !!}

                    <hr>
                    @if($post->comments->count())
                        <h4>Comments</h4>
                        <hr>
                    @endif
                    <div class="row">
                        @foreach($post->comments as $comment)
                            <div class="post-preview">
                                <p class="post-meta">
                                    {!! $comment->comment !!}
                                    <br>
                                    Comment by
                                    <a href="#">{{ $post->owner->name }}</a>
                                    on {{ \Carbon\Carbon::parse($comment->created_at)->format('Y-m-d') }}

                                    @auth
                                        @if(Auth::user()->id == $comment->user_id)
                                            <a class="m-2 text-danger" href="#" data-toggle="modal" data-target="#comment_delete_modal"><i class="fa fa-trash"></i></a>
                                        @endif
                                    @endauth
                                </p>
                            </div>
                            @include('modals.comment-delete-modal')

                        @endforeach
                    </div>
                    <hr>
                    @auth
                        <form class="mt-5" action="{{ route('comments.store') }}" method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="post_id" value="{{ encodeId($post->id) }}">
                            <div class="form-group">
                                <label>Leave a comment</label>
                                <textarea id="summernote" rows="6" name="comment" class="form-control"> {{ old('comment') }}</textarea>
                                @if ($errors->has('comment'))
                                    <span class="has-error text-danger small">{{ $errors->first('comment') }}</span>
                                @endif
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    @else
                        Want to leave a comment? <a href="{{ route('login') }}">Login</a>
                    @endauth
                </div>
            </div>
        </div>
    </article>

    <hr>
@endsection