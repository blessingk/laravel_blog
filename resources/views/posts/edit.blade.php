@extends('layouts.app')
@section('content')
    <!-- Main Content -->
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
                <div class="post-preview">
                    <h2 class="post-title">
                        Update Post
                    </h2>
                    <form action="{{ route('posts.update', encodeId($post->id)) }}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" name="title" value="{{ $post->title }}" class="form-control" id="title">
                            @if ($errors->has('title'))
                                <span class="has-error text-danger small">{{ $errors->first('title') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Content</label>
                            <textarea id="summernote" rows="6" name="contents" class="form-control"> {{ $post->content }}</textarea>
                            @if ($errors->has('contents'))
                                <span class="has-error text-danger small">{{ $errors->first('contents') }}</span>
                            @endif
                        </div>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </form>
                </div>
                <hr>
            </div>
        </div>
    </div>

    <hr>
@endsection