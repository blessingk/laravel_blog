<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert as swal;

class UserController extends Controller
{

    public function edit()
    {
        $user = Auth::user();
        return view('users.edit', compact('user'));
    }


    public function update(Request $request)
    {
        try {
            $user = Auth::user();
            $user->name = $request->name;
            $user->email = $request->email;

            if(!empty($request->password)) {
                $user->password = bcrypt($request->password);
            }

            $user->update();

            swal::success('Profile updated')->toToast();
            return back();
        } catch (\Exception $exception) {
            swal::error('Oops... Failed to update profile')->toToast();
            return back();
        }
    }
}
