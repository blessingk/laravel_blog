<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Post\PostRepositoryInterface;
use RealRashid\SweetAlert\Facades\Alert as swal;

class PostController extends Controller
{
    /**
     * @var PostRepositoryInterface
     */
    private $postRepository;

    public function __construct(PostRepositoryInterface $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    public function index()
    {
        $posts = $this->postRepository->getAll();
        return view('posts.index', compact('posts'));
    }

    public function create()
    {
        return view('posts.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'contents' => 'required',
        ]);

        try{
            $this->postRepository->store($request);
            swal::success('Post created successfully')->toToast();
            return redirect('/');
        } catch(\Exception $e){
            swal::error('Oops Failed to create post')->toToast();
            return back();
        }
    }

    public function show($encodedId)
    {
        $id = decodeId($encodedId);
        $post = $this->postRepository->getById($id);
        return view('posts.show', compact('post'));
    }

    public function edit($encodedId)
    {
        $id = decodeId($encodedId);
        $post = $this->postRepository->getById($id);
        return view('posts.edit', compact('post'));
    }

    public function update(Request $request, $encodedId)
    {
        $this->validate($request, [
            'title' => 'required',
            'contents' => 'required',
        ]);

        try{
            $id = decodeId($encodedId);
            $this->postRepository->update($request, $id);
            swal::success('Post updated successfully')->toToast();
            return redirect('/');
        } catch(\Exception $e){
            swal::error('Oops Failed to update post')->toToast();
            return back();
        }
    }

    public function destroy($encodedId)
    {
        try {
            $id = decodeId($encodedId);
            $this->postRepository->destroy($id);
            swal::success('Post deleted successfully')->toToast();
            return redirect('/');
        } catch(\Exception $e){
            swal::error('Oops Failed to delete post')->toToast();
            return back();
        }
    }
}
