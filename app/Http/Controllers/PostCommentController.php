<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\PostComment\PostCommentRepositoryInterface;
use RealRashid\SweetAlert\Facades\Alert as swal;

class PostCommentController extends Controller
{
    /**
     * @var PostCommentRepositoryInterface
     */
    private $postCommentRepository;

    public function __construct(PostCommentRepositoryInterface $postCommentRepository)
    {
        $this->postCommentRepository = $postCommentRepository;
    }

    public function store(Request $request)
    {
        $request->validate([
            'comment' => 'required',
        ]);

        try{
            $this->postCommentRepository->store($request);
            swal::success('Comment submitted successfully')->toToast();
            return back();
        } catch(\Exception $e){
            swal::error('Oops Failed to create comment')->toToast();
            return back();
        }
    }

    public function destroy($encodedId)
    {
        try {
            $id = decodeId($encodedId);
            $this->postCommentRepository->destroy($id);
            swal::success('Comment deleted successfully')->toToast();
            return back();
        } catch(\Exception $e){
            swal::error('Oops Failed to delete comment')->toToast();
            return back();
        }
    }
}
