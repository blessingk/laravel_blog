<?php

namespace App\Repositories\PostComment;

use App\Models\PostComment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostCommentRepository implements PostCommentRepositoryInterface
{
    /**
     * @var PostComment
     */
    private $postComment;

    public function __construct(PostComment $postComment)
    {
        $this->postComment = $postComment;
    }

    public function store(Request $request)
    {
        $post_id = decodeId($request->post_id);
        $comment = new $this->postComment;
        $comment->post_id = $post_id;
        $comment->user_id = Auth::user()->id;
        $comment->comment = $request->comment;
        $comment->save();
        return $comment;

    }

    public function destroy($id)
    {
        return $this->postComment->where('id', $id)->delete();
    }
}