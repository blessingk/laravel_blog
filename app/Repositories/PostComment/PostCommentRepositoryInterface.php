<?php

namespace App\Repositories\PostComment;

use Illuminate\Http\Request;

interface PostCommentRepositoryInterface
{
    public function store(Request $request);

    public function destroy($id);
}