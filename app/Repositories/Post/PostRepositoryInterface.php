<?php

namespace App\Repositories\Post;

use Illuminate\Http\Request;

interface PostRepositoryInterface
{
    public function getAll();

    public function getById($id);

    public function update(Request $request, $id);

    public function store(Request $request);

    public function destroy($id);

}