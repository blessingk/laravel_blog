<?php

namespace App\Repositories\Post;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostRepository implements PostRepositoryInterface
{

    /**
     * @var Post
     */
    private $post;

    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    public function getAll()
    {
        return $this->post->latest()->get();
    }

    public function getById($id)
    {
        return $this->post->findOrFail($id);
    }

    public function update(Request $request, $id)
    {
        $post = $this->post->findOrFail($id);
        $post->title = $request->title;
        $post->content = $request->contents;
        $post->update();
        return $post;
    }

    public function store(Request $request)
    {
        $post = new $this->post;
        $post->user_id = Auth::user()->id;
        $post->title = $request->title;
        $post->content = $request->contents;
        $post->save();
        return $post;

    }

    public function destroy($id)
    {
        return $this->post->where('id', $id)->delete();
    }
}