<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
/*
|--------------------------------------------------------------------------
| Posts Routes
|--------------------------------------------------------------------------
*/
Route::get('/', 'PostController@index')->name('posts.index');
Route::get('/posts/create', 'PostController@create')->name('posts.create')->middleware('auth');
Route::get('/posts/{id}', 'PostController@show')->name('posts.show');
Route::get('/posts/{id}/edit', 'PostController@edit')->name('posts.edit')->middleware('auth');
Route::post('/posts/store', 'PostController@store')->name('posts.store')->middleware('auth');
Route::post('/posts/{id}/update', 'PostController@update')->name('posts.update')->middleware('auth');
Route::get('/posts/{id}/delete', 'PostController@destroy')->name('posts.destroy')->middleware('auth');

/*
|--------------------------------------------------------------------------
| Comments Routes
|--------------------------------------------------------------------------
*/
Route::post('/comments/store', 'PostCommentController@store')->name('comments.store')->middleware('auth');
Route::get('/comments/{id}/delete', 'PostCommentController@destroy')->name('comments.destroy')->middleware('auth');

/*
|--------------------------------------------------------------------------
| Users Routes
|--------------------------------------------------------------------------
*/
Route::post('/users/update', 'UserController@update')->name('users.update')->middleware('auth');
Route::get('/users/edit', 'UserController@edit')->name('users.edit')->middleware('auth');
