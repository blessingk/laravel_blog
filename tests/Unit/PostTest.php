<?php

namespace Tests\Unit;

use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PostTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreatePost()
    {
        $user = factory(User::class)->create();

        // Guests
        $this->get('/posts/create')->assertRedirect('login');

        // Users
        $this->actingAs($user)->get('/posts/create')->assertStatus(200);
    }
}
